#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "terrain.h"
#include "terrain_format.h"
#include "test.h"
#include "solver.h"

int cursor_x = 0;
int cursor_y = 0;
terrain actual;

const int base_test[9][9] =
    { {0,5,2, 0,0,6, 0,0,0},
      {1,6,0, 9,0,0, 0,0,4},
      {0,4,9, 8,0,3, 6,2,0},

      {4,0,0, 0,0,0, 8,0,0},
      {0,8,3, 2,0,1, 5,9,0},
      {0,0,1, 0,0,0, 0,0,2},

      {0,9,7, 3,0,5, 2,4,0},
      {2,0,0, 0,0,9, 0,5,6},
      {0,0,0, 1,0,0, 9,7,0},
};

void move_cursor(int c) {
    switch(c) {
    case KEY_DOWN:
        cursor_y++;
        break;
    case KEY_UP:
        cursor_y--;
        break;
    case KEY_RIGHT:
        cursor_x++;
        break;
    case KEY_LEFT:
        cursor_x--;
        break;
    default:
        return;
    }
    if(cursor_x < 0) cursor_x = 0;
    if(cursor_y < 0) cursor_y = 0;
    if(cursor_x > 8) cursor_x = 8;
    if(cursor_y > 8) cursor_y = 8;

    move(cursor_y*4+2 + cursor_y/3,cursor_x*4+2 + cursor_x/3);
}
int choose_number(int c) {
    if(c >= '1' && c <= '9')
        case_set(&actual, cursor_x,cursor_y,c-'0');
    else if(c == KEY_DC || c == KEY_BACKSPACE)
        case_set(&actual, cursor_x,cursor_y,0);
    else
        return 0;

    test_case(&actual, cursor_x, cursor_y);

    return 1;
}
int main() {
    int modified = 0;

    initscr();
    noecho();
    keypad(stdscr,TRUE);
    curs_set(2);
    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);

    memcpy(actual.terrain, base_test, sizeof(int) * 81);

    terrain_format_init();
    terrain_format_print();
    terrain_print(actual);

    while(1) {
        move(cursor_y*4+2 + cursor_y/3,cursor_x*4+2 + cursor_x/3);

        int c = getch();
        move(5,45);
        clrtoeol();

        move_cursor(c);
        modified = choose_number(c);

        if(c == 's') {
            solve(&actual);
            terrain_print(actual);
        }
        if(c == 'q')
            break;
        if(modified)
            terrain_print(actual);
        if(modified && test_end(&actual))
            break;
        refresh();
    }
    clrtoeol();
    endwin();
}
