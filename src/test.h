#ifndef _TEST_H_
#define _TEST_H_

#include "terrain.h"

extern int test_horizontal(terrain* t, int y);
extern int test_vertical  (terrain* t, int x);
extern int test_square    (terrain* t, int x, int y);
extern int test_case      (terrain* t, int x, int y);
extern int test_end       (terrain* t);

#endif /* _TEST_H_ */
