#include "terrain_format.h"
#include <string.h>

chtype terrain_format[39][39];

void terrain_format_init() {
    /* Initialize the terrain */
    for(int x = 0; x < 39; x++)
        for(int y = 0; y < 39; y++)
            terrain_format[y][x] = ' ';

    /* Add the vertical lines */
    for(int i = 0; i < 3; i++)
        for(int x = 0; x < 4; x++)
            for(int y = 0; y < 39; y++)
                terrain_format[y][i*13 + 4*(x-x/4)] = ACS_VLINE;

    /* Add the horizontal lines */
    for(int i = 0; i < 3; i++)
        for(int y = 0; y < 4; y++)
            for(int x = 0; x < 39; x++)
                terrain_format[i*13 + 4*(y-y/4)][x] = ACS_HLINE;

    /* Add the upper left corners */
    for(int x = 0; x < 3; x++)
        for(int y = 0; y < 3; y++)
            terrain_format[y*12+y][x*12+x] = ACS_ULCORNER;

    /* Add the upper right corners */
    for(int x = 0; x < 3; x++)
        for(int y = 0; y < 3; y++)
            terrain_format[y*12+y][x*12+x+12] = ACS_URCORNER;

    /* Add the lower left corners */
    for(int x = 0; x < 3; x++)
        for(int y = 0; y < 3; y++)
            terrain_format[y*12+y+12][x*12+x] = ACS_LLCORNER;

    /* Add the lower right corners */
    for(int x = 0; x < 3; x++)
        for(int y = 0; y < 3; y++)
            terrain_format[y*12+y+12][x*12+x+12] = ACS_LRCORNER;

    /* Add the lower right corners */
    for(int x = 0; x < 3; x++)
        for(int i = 0; i < 3; i++)
            for(int y = 0; y < 2; y++)
                terrain_format[y*4+4+i*13][x*12+x] = ACS_LTEE;

    /* Add the lower right corners */
    for(int x = 0; x < 3; x++)
        for(int i = 0; i < 3; i++)
            for(int y = 0; y < 2; y++)
                terrain_format[y*4+4+i*13][x*12+x+12] = ACS_RTEE;

    /* Add the crosses */
    for(int x = 0; x < 6; x++)
        for(int y = 0; y < 6; y++)
            terrain_format[4*(y+1)+(y/2)*5][4*(x+1)+(x/2)*5] = ACS_PLUS;

    /* Add the down T */
    for(int x = 0; x < 6; x++)
        for(int y = 0; y < 3; y++)
            terrain_format[y*13][4*(x+1)+(x/2)*5] = ACS_TTEE;

    /* Add the up T */
    for(int x = 0; x < 6; x++)
        for(int y = 0; y < 3; y++)
            terrain_format[y*13+12][4*(x+1)+(x/2)*5] = ACS_BTEE;
}
void terrain_format_print() {
    for(int x = 0; x < 39; x++)
        for(int y = 0; y < 39; y++)
            mvaddch(y,x, terrain_format[y][x]);
}
