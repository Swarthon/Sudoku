#ifndef _TERRAIN_H_
#define _TERRAIN_H_

struct terrain {
    int terrain[9][9];  /* Base terrain : unmodifiable cases */
    int layer  [9][9];  /* Cases added by the user */

    /* Actual errors */
    int errors_v[9][9];
    int errors_h[9][9];
    int errors_s[9][9];
};
typedef struct terrain terrain;

/*--------------------------------- ERROR -----------------------------------*/
enum error_type {
        HORIZONTAL,
        VERTICAL,
        SQUARE
};

extern void add_error(terrain* t, int x,int y, enum error_type e);
extern void del_error(terrain* t, int x,int y, enum error_type e);
extern int  get_error(terrain  t, int x,int y, enum error_type e);

#define add_error_h(t,x,y) add_error(t,x,y,HORIZONTAL)
#define add_error_v(t,x,y) add_error(t,x,y,VERTICAL)
#define add_error_s(t,x,y) add_error(t,x,y,SQUARE)

#define get_error_h(t,x,y) get_error(t,x,y,HORIZONTAL)
#define get_error_v(t,x,y) get_error(t,x,y,VERTICAL)
#define get_error_s(t,x,y) get_error(t,x,y,SQUARE)

#define del_error_h(t,x,y) del_error(t,x,y,HORIZONTAL)
#define del_error_v(t,x,y) del_error(t,x,y,VERTICAL)
#define del_error_s(t,x,y) del_error(t,x,y,SQUARE)
/*---------------------------------------------------------------------------*/

extern void case_set(terrain* t, int x, int y, int i);
extern int  case_get(terrain  t, int x, int y);
extern char case_convert(int i);

extern void terrain_print(terrain t);

#endif /* _TERRAIN_H_ */
