#include "solver.h"

#include "terrain.h"
#include "test.h"

void get_first_void_case(terrain *t, int *x, int *y) {
    for(*x = 0; (*x) < 9; (*x)++)
        for(*y = 0; (*y) < 9; (*y)++)
            if(case_get(*t, *x, *y) == 0)
                return;
}
int solve(terrain *t) {
    int x = 0, y = 0;

    get_first_void_case(t, &x, &y);
    if(x == 9)
        return test_end(t);
    for(int i = 1; i <= 9; i++) {
        case_set(t,x,y,i);
        if(!test_case(t,x,y))
            continue;
        if(solve(t) == 1)
            return 1;
    }
    case_set(t,x,y,0);
    return 0;
}
