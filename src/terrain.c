#include "terrain.h"

#include <ncurses.h>

void case_set(terrain* t, int x, int y, int i) {
    if(t->terrain[y][x] == 0)
        t->layer[y][x] = i;
}
int case_get(terrain t, int x, int y) {
    if(t.terrain[y][x] == 0)
        return t.layer[y][x];
    return t.terrain[y][x];
}
char case_convert(int i) {
    if(i == 0)
        return ' ';
    else {
        char buf [2];
        sprintf(buf, "%d", i);
        return buf[0];
    }
}

void add_error(terrain* t, int x, int y, enum error_type e) {
    if(e == VERTICAL)   t->errors_v[y][x] = 1;
    if(e == HORIZONTAL) t->errors_h[y][x] = 1;
    if(e == SQUARE)     t->errors_s[y][x] = 1;
}
void del_error(terrain* t, int x, int y, enum error_type e) {
    if(e == VERTICAL)   t->errors_v[y][x] = 0;
    if(e == HORIZONTAL) t->errors_h[y][x] = 0;
    if(e == SQUARE)     t->errors_s[y][x] = 0;
}
int get_error(terrain t, int x, int y, enum error_type e) {
    if(e == VERTICAL)   return t.errors_v[y][x];
    if(e == HORIZONTAL) return t.errors_h[y][x];
    return t.errors_s[y][x];
}

void terrain_print(terrain t) {
    for(int x = 0; x < 9; x++) {
        for(int y = 0; y < 9; y++) {
            if(get_error_v(t,x,y) || get_error_h(t,x,y) || get_error_s(t,x,y))
                attron(COLOR_PAIR(1));
            mvaddch(y*4+2 + y/3,x*4+2 + x/3,case_convert(case_get(t,x,y)));
            if(get_error_v(t,x,y) || get_error_h(t,x,y) || get_error_s(t,x,y))
                attroff(COLOR_PAIR(1));
        }
    }
}
