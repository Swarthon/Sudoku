#ifndef _SOLVER_H_
#define _SOLVER_H_

#include "terrain.h"

extern int solve(terrain* t);

#endif /* _SOLVER_H_ */
