#include "test.h"

#include <stdio.h>

int test_horizontal(terrain* t, int y) {
    int ret = 1;
    short array[10] = {0,0,0,0,0,0,0,0,0,0};
    for(int i = 0; i < 9; i++)
        array[case_get(*t,i,y)]++;
    for(int i = 0; i < 9; i++) {
        if(array[case_get(*t,i,y)] > 1 && case_get(*t,i,y)) {
            add_error_h(t,i,y);
            ret = 0;
        }
        else if(get_error_h(*t,i,y))
            del_error_h(t,i,y);
    }
    return ret;
}
int test_vertical(terrain* t, int x) {
    int ret = 1;
    short array[10] = {0,0,0,0,0,0,0,0,0,0};
    for(int i = 0; i < 9; i++)
        array[case_get(*t,x,i)]++;
    for(int i = 0; i < 9; i++) {
        if(array[case_get(*t,x,i)] > 1 && case_get(*t,x,i)) {
            add_error_v(t,x,i);
            ret = 0;
        }
        else if(get_error_v(*t,x,i))
            del_error_v(t,x,i);
    }
    return ret;
}
int test_square(terrain* t, int x, int y) {
    int ret = 1;
    int array[10] = {0,0,0,0,0,0,0,0,0,0};
    int xs = (int)(x / 3) * 3;
    int ys = (int)(y / 3) * 3;
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
            array[case_get(*t,xs+i,ys+j)]++;
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++) {
            if(array[case_get(*t,xs+i,ys+j)] > 1 && case_get(*t,xs+i,ys+j)) {
                add_error_s(t,xs+i,ys+j);
                ret = 0;
            }
            else if(get_error_s(*t,xs+i,ys+j))
                del_error_s(t,xs+i,ys+j);
        }
    return ret;
}
int test_case(terrain* t, int x, int y) {
    if(!test_horizontal(t,y))
        return 0;
    if(!test_vertical(t,x))
        return 0;
    if(!test_square(t,x,y))
        return 0;
    return 1;
}
int test_end(terrain* t) {
    for(int y = 0; y < 9; y++)
        if(test_horizontal(t,y) == 0)
            return 0;
    for(int x = 0; x < 9; x++)
        if(test_vertical(t,x) == 0)
            return 0;
    for(int x = 0; x < 3; x++)
        for(int y = 0; y < 3; y++)
            if(test_square(t,x*3,y*3) == 0)
                return 0;
    for(int x = 0; x < 9; x++)
        for(int y = 0; y < 9; y++)
            if(case_get(*t,x,y) == 0)
                return 0;
    return 1;
}
